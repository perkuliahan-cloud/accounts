<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('users', 'UserController@index')->name('index.user');
Route::get('user/{id}', 'UserController@show')->name('show.user');
Route::post('user/store', 'UserController@store')->name('store.user');
Route::post('user/update/{id}', 'UserController@update')->name('update.user');
Route::post('user/delete', 'UserController@delete')->name('delete.user');

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
