<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $gender = $faker->randomElement(['LAKI-LAKI', 'PEREMPUAN']);
    $jenis = $faker->randomElement(['MAHASISWA', 'DOSEN', 'TU']);
    $nm = ($gender=="LAKI-LAKI")?"male":"female";
    return [
        'nomor_induk'=>$faker->numerify("##########"),
        'nama_awal'=>$faker->firstName($nm),
        'nama_belakang'=>$faker->lastName,
        'tempat_lahir'=>$faker->streetName,
        'tanggal_lahir'=>$faker->date('Y-m-d', '-20 years'),
        'gender'=>$gender,
        'alamat'=>$faker->streetAddress,
        'no_telp'=>$faker->phoneNumber,
        'jenis_user'=>$jenis,
        'status_user'=>"AKTIF",
        'password'=>\Illuminate\Support\Facades\Hash::make('password')
    ];
});
