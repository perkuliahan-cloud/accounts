<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomor_induk', 12);
            $table->string('nama_awal', 30);
            $table->string('nama_belakang', 50);
            $table->string('tempat_lahir', 50);
            $table->date('tanggal_lahir');
            $table->enum('gender', ['LAKI-LAKI', 'PEREMPUAN']);
            $table->string('alamat');
            $table->string('no_telp', 20);
            $table->string('photo_profile')->nullable()->default("default.png");
            $table->string('password');
            $table->enum('jenis_user', ['MAHASISWA', 'DOSEN', 'TU'])->default('MAHASISWA');
            $table->enum('status_user', ['AKTIF', 'NON-AKTIF'])->default('AKTIF');
            $table->dateTime('last_login')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
