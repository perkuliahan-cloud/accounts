<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;
use Illuminate\Support\Facades\Storage;

class User extends Model
{
    protected $table = 'users';
    protected $fillable = [
        'nomor_induk',
        'nama_awal',
        'nama_belakang',
        'tempat_lahir',
        'tanggal_lahir',
        'gender',
        'alamat',
        'no_telp',
        'photo_profile',
        'jenis_user',
        'status_user'
    ];
    protected $hidden = ['password'];
    protected $appends = ['usia', 'url_photo'];

    public function getUsiaAttribute(){
        $bday = new DateTime($this->tanggal_lahir); // Your date of birth
        $today = new Datetime(date('Y-m-d'));
        $diff = $today->diff($bday);
        return $diff->y;
    }

    public function getUrlPhotoAttribute(){
        return Storage::url('user_profile/' . $this->photo_profile);
    }
}
