<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UtilityController extends Controller
{
    public function uploadFoto($file){
        $ext = $file->extension();
        $imgName = date('dmYHis').'.'.$ext;
        Storage::putFileAs('user_profile/', $file, $imgName);
        return $imgName;
    }

    public function updateFoto($old, $new){
        if($old != "default.png"){
            Storage::delete('user_profile/'.$old);
        }
        $ext = $new->extension();
        $imgName = date('dmYHis').'.'.$ext;
        Storage::putFileAs('user_profile/', $new, $imgName);
        return $imgName;
    }

    public function destroyFoto($file){
        if($file != "default.png"){
            Storage::delete('user_profile/'.$file);
        }
    }

}
