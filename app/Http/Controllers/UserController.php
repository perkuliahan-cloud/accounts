<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\UtilityController;

class UserController extends Controller
{
    protected $utility;
    public function __construct(UtilityController $utility){
        $this->utility = $utility;
    }

    public function index(){
        $usr = User::all();
        return response()->json(['code'=>200,'error'=>false,'data'=>$usr]);
    }

    public function show($id){
        $user = User::find($id);
        return response()->json(['code'=>200, 'error'=>false, 'data'=>$user]);
    }

    public function store(Request $request){
        DB::beginTransaction();
        $usr = new User();
        $usr->nomor_induk = $request->post('nomor_induk');
        $usr->nama_awal = $request->post('nama_awal');
        $usr->nama_belakang = $request->post('nama_belakang');
        $usr->tempat_lahir = $request->post('tempat_lahir');
        $usr->tanggal_lahir = $request->post('tanggal_lahir');
        $usr->gender = $request->post('gender');
        $usr->alamat = $request->post('alamat');
        $usr->no_telp = $request->post('no_telp');
        $usr->password = Hash::make($request->post('password'));
        $usr->jenis_user = $request->post('jenis_user');
        if($request->file('photo') != null || $request->file('photo') != ""){
            $usr->photo_profile = $this->utility->uploadFoto($request->file('photo'));
        }else{
            $usr->photo_profile = "default.png";
        }
        $usr->save();
        DB::commit();
        return response()->json(['code'=>201,'error'=>false,'data'=>$usr]);
    }

    public function update(Request $request, $id){
        DB::beginTransaction();
        $usr = User::find($id);
        $usr->nomor_induk = $request->post('nomor_induk');
        $usr->nama_awal = $request->post('nama_awal');
        $usr->nama_belakang = $request->post('nama_belakang');
        $usr->tempat_lahir = $request->post('tempat_lahir');
        $usr->tanggal_lahir = $request->post('tanggal_lahir');
        $usr->gender = $request->post('gender');
        $usr->alamat = $request->post('alamat');
        $usr->no_telp = $request->post('no_telp');
        $usr->password = Hash::make($request->post('password'));
        $usr->jenis_user = $request->post('jenis_user');
        if($request->file('photo') != null || $request->file('photo') != ""){
            $usr->photo_profile = $this->utility->updateFoto($usr->photo_profile, $request->file('photo'));
        }
        $usr->save();
        DB::commit();
        return response()->json(['code'=>200,'error'=>false,'data'=>$usr]);
    }

    public function delete(Request $request){
        DB::beginTransaction();
        $usr = User::find($request->post('id'));
        $this->utility->destroyFoto($usr->photo_profile);
        $usr->delete();
        DB::commit();
        return response()->json(['code'=>200,'error'=>false,'msg'=>'data is deleted']);
    }

}
